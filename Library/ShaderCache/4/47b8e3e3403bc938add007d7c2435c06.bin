<Q                         _ADDITIONAL_LIGHTS_VERTEX       8  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0_0_0[4];
uniform 	mediump vec4 _AdditionalLightsCount;
uniform 	vec4 _AdditionalLightsPosition[32];
uniform 	mediump vec4 _AdditionalLightsColor[32];
uniform 	mediump vec4 _AdditionalLightsAttenuation[32];
uniform 	mediump vec4 _AdditionalLightsSpotDir[32];
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec3 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TANGENT0;
in highp vec4 in_TEXCOORD0;
out highp vec3 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD4;
out highp vec2 vs_TEXCOORD5;
out highp vec3 vs_TEXCOORD6;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD8;
vec3 u_xlat0;
vec3 u_xlat1;
vec4 u_xlat2;
mediump vec4 u_xlat16_2;
mediump vec3 u_xlat16_3;
int u_xlati4;
bool u_xlatb4;
float u_xlat5;
mediump vec3 u_xlat16_6;
vec3 u_xlat11;
uint u_xlatu11;
float u_xlat12;
float u_xlat21;
uint u_xlatu21;
uint u_xlatu22;
mediump float u_xlat16_24;
void main()
{
	ImmCB_0_0_0[0] = vec4(1.0, 0.0, 0.0, 0.0);
	ImmCB_0_0_0[1] = vec4(0.0, 1.0, 0.0, 0.0);
	ImmCB_0_0_0[2] = vec4(0.0, 0.0, 1.0, 0.0);
	ImmCB_0_0_0[3] = vec4(0.0, 0.0, 0.0, 1.0);
    u_xlat0.xyz = in_POSITION0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * in_POSITION0.xxx + u_xlat0.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * in_POSITION0.zzz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat21 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat21 = max(u_xlat21, 1.17549435e-38);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat1.xyz;
    u_xlat2.x = hlslcc_mtx4x4unity_ObjectToWorld[0].x;
    u_xlat2.y = hlslcc_mtx4x4unity_ObjectToWorld[1].x;
    u_xlat2.z = hlslcc_mtx4x4unity_ObjectToWorld[2].x;
    u_xlat16_3.x = dot(u_xlat2.xyz, in_TANGENT0.xyz);
    u_xlat2.x = hlslcc_mtx4x4unity_ObjectToWorld[0].y;
    u_xlat2.y = hlslcc_mtx4x4unity_ObjectToWorld[1].y;
    u_xlat2.z = hlslcc_mtx4x4unity_ObjectToWorld[2].y;
    u_xlat16_3.y = dot(u_xlat2.xyz, in_TANGENT0.xyz);
    u_xlat2.x = hlslcc_mtx4x4unity_ObjectToWorld[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_ObjectToWorld[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_ObjectToWorld[2].z;
    u_xlat16_3.z = dot(u_xlat2.xyz, in_TANGENT0.xyz);
    u_xlat21 = dot(u_xlat16_3.xyz, u_xlat16_3.xyz);
    u_xlat21 = max(u_xlat21, 1.17549435e-38);
    u_xlat21 = inversesqrt(u_xlat21);
    vs_TEXCOORD2.xyz = vec3(u_xlat21) * u_xlat16_3.xyz;
    u_xlat2 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat2;
    gl_Position = u_xlat2 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD4.xyz = (-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat16_2 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_3.x = dot(unity_SHBr, u_xlat16_2);
    u_xlat16_3.y = dot(unity_SHBg, u_xlat16_2);
    u_xlat16_3.z = dot(unity_SHBb, u_xlat16_2);
    u_xlat16_24 = u_xlat1.y * u_xlat1.y;
    u_xlat16_24 = u_xlat1.x * u_xlat1.x + (-u_xlat16_24);
    u_xlat16_3.xyz = unity_SHC.xyz * vec3(u_xlat16_24) + u_xlat16_3.xyz;
    u_xlat16_24 = min(_AdditionalLightsCount.x, unity_LightData.y);
    u_xlatu21 = uint(int(u_xlat16_24));
    u_xlat16_2.y = float(0.0);
    u_xlat16_2.z = float(0.0);
    u_xlat16_2.w = float(0.0);
    for(uint u_xlatu_loop_1 = uint(0u) ; u_xlatu_loop_1<u_xlatu21 ; u_xlatu_loop_1++)
    {
        u_xlati4 = int(uint(u_xlatu_loop_1 & 3u));
        u_xlatu11 = uint(u_xlatu_loop_1 >> 2u);
        u_xlat16_24 = dot(unity_LightIndices[int(u_xlatu11)], ImmCB_0_0_0[u_xlati4]);
        u_xlati4 = int(u_xlat16_24);
        u_xlat11.xyz = (-u_xlat0.xyz) * _AdditionalLightsPosition[u_xlati4].www + _AdditionalLightsPosition[u_xlati4].xyz;
        u_xlat5 = dot(u_xlat11.xyz, u_xlat11.xyz);
        u_xlat5 = max(u_xlat5, 6.10351563e-05);
        u_xlat12 = inversesqrt(u_xlat5);
        u_xlat11.xyz = u_xlat11.xyz * vec3(u_xlat12);
        u_xlat12 = float(1.0) / float(u_xlat5);
        u_xlat5 = u_xlat5 * _AdditionalLightsAttenuation[u_xlati4].x + _AdditionalLightsAttenuation[u_xlati4].y;
#ifdef UNITY_ADRENO_ES3
        u_xlat5 = min(max(u_xlat5, 0.0), 1.0);
#else
        u_xlat5 = clamp(u_xlat5, 0.0, 1.0);
#endif
        u_xlat5 = u_xlat5 * u_xlat12;
        u_xlat16_24 = dot(_AdditionalLightsSpotDir[u_xlati4].xyz, u_xlat11.xyz);
        u_xlat16_24 = u_xlat16_24 * _AdditionalLightsAttenuation[u_xlati4].z + _AdditionalLightsAttenuation[u_xlati4].w;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_24 = min(max(u_xlat16_24, 0.0), 1.0);
#else
        u_xlat16_24 = clamp(u_xlat16_24, 0.0, 1.0);
#endif
        u_xlat16_24 = u_xlat16_24 * u_xlat16_24;
        u_xlat5 = u_xlat16_24 * u_xlat5;
        u_xlat16_6.xyz = vec3(u_xlat5) * _AdditionalLightsColor[u_xlati4].xyz;
        u_xlat16_24 = dot(u_xlat1.xyz, u_xlat11.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat16_24 = min(max(u_xlat16_24, 0.0), 1.0);
#else
        u_xlat16_24 = clamp(u_xlat16_24, 0.0, 1.0);
#endif
        u_xlat16_2.yzw = u_xlat16_6.xyz * vec3(u_xlat16_24) + u_xlat16_2.yzw;
    }
    vs_TEXCOORD2.w = in_TANGENT0.w;
    vs_TEXCOORD3 = in_TEXCOORD0;
    u_xlat16_2.x = 0.0;
    vs_TEXCOORD7 = u_xlat16_2;
    vs_TEXCOORD8 = vec4(0.0, 0.0, 0.0, 0.0);
    vs_TEXCOORD0.xyz = u_xlat0.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    vs_TEXCOORD6.xyz = u_xlat16_3.xyz;
    vs_TEXCOORD5.xy = vec2(0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _MainLightPosition;
uniform 	mediump vec4 _MainLightColor;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM float Vector1_FFCA610B;
	UNITY_UNIFORM float Vector1_E53E3416;
	UNITY_UNIFORM float Vector1_3D95EAF7;
	UNITY_UNIFORM float Vector1_1B30607B;
	UNITY_UNIFORM float Vector1_D45CEC2F;
	UNITY_UNIFORM float Vector1_92BCA7DF;
	UNITY_UNIFORM float Vector1_4F55B5A9;
	UNITY_UNIFORM float Vector1_7693A0B9;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump samplerCube unity_SpecCube0;
UNITY_LOCATION(1) uniform mediump sampler2D Texture2D_7005A439;
UNITY_LOCATION(2) uniform mediump sampler2D Texture2D_5A3D6B5D;
in highp vec3 vs_TEXCOORD1;
in highp vec4 vs_TEXCOORD2;
in highp vec4 vs_TEXCOORD3;
in highp vec3 vs_TEXCOORD4;
in highp vec3 vs_TEXCOORD6;
in highp vec4 vs_TEXCOORD7;
layout(location = 0) out mediump vec4 SV_TARGET0;
vec3 u_xlat0;
mediump vec4 u_xlat16_0;
bool u_xlatb0;
mediump vec3 u_xlat16_1;
vec3 u_xlat2;
mediump vec4 u_xlat16_2;
vec3 u_xlat3;
mediump vec3 u_xlat16_3;
vec2 u_xlat4;
mediump vec3 u_xlat16_4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
vec3 u_xlat9;
mediump float u_xlat16_22;
float u_xlat23;
void main()
{
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(0.0<vs_TEXCOORD2.w);
#else
    u_xlatb0 = 0.0<vs_TEXCOORD2.w;
#endif
    u_xlat0.x = (u_xlatb0) ? 1.0 : -1.0;
    u_xlat16_1.x = u_xlat0.x * unity_WorldTransformParams.w;
    u_xlat0.xyz = vs_TEXCOORD1.zxy * vs_TEXCOORD2.yzx;
    u_xlat0.xyz = vs_TEXCOORD1.yzx * vs_TEXCOORD2.zxy + (-u_xlat0.xyz);
    u_xlat0.xyz = u_xlat0.xzy * u_xlat16_1.xxx;
    u_xlat2.y = u_xlat0.x;
    u_xlat2.x = vs_TEXCOORD2.x;
    u_xlat2.z = vs_TEXCOORD1.x;
    u_xlat3.xy = vs_TEXCOORD3.xy * vec2(Vector1_D45CEC2F, Vector1_92BCA7DF) + vec2(Vector1_4F55B5A9, Vector1_7693A0B9);
    u_xlat16_3.xyz = texture(Texture2D_5A3D6B5D, u_xlat3.xy).xyz;
    u_xlat16_1.x = dot(u_xlat16_3.xyz, u_xlat2.xyz);
    u_xlat2.y = u_xlat0.z;
    u_xlat2.x = vs_TEXCOORD2.y;
    u_xlat2.z = vs_TEXCOORD1.y;
    u_xlat16_1.y = dot(u_xlat16_3.xyz, u_xlat2.xyz);
    u_xlat0.x = vs_TEXCOORD2.z;
    u_xlat0.z = vs_TEXCOORD1.z;
    u_xlat16_1.z = dot(u_xlat16_3.xyz, u_xlat0.xyz);
    u_xlat16_22 = dot(u_xlat16_1.xyz, u_xlat16_1.xyz);
    u_xlat16_22 = inversesqrt(u_xlat16_22);
    u_xlat16_0.xyz = vec3(u_xlat16_22) * u_xlat16_1.xyz;
    u_xlat2.x = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat2.x = max(u_xlat2.x, 1.17549435e-38);
    u_xlat2.x = inversesqrt(u_xlat2.x);
    u_xlat9.xyz = u_xlat2.xxx * vs_TEXCOORD4.xyz;
    u_xlat3.xyz = vs_TEXCOORD4.xyz * u_xlat2.xxx + _MainLightPosition.xyz;
    u_xlat16_1.x = dot((-u_xlat9.xyz), u_xlat16_0.xyz);
    u_xlat16_1.x = u_xlat16_1.x + u_xlat16_1.x;
    u_xlat16_1.xyz = u_xlat16_0.xyz * (-u_xlat16_1.xxx) + (-u_xlat9.xyz);
    u_xlat16_22 = dot(u_xlat16_0.xyz, u_xlat9.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_22 = min(max(u_xlat16_22, 0.0), 1.0);
#else
    u_xlat16_22 = clamp(u_xlat16_22, 0.0, 1.0);
#endif
    u_xlat16_22 = (-u_xlat16_22) + 1.0;
    u_xlat16_22 = u_xlat16_22 * u_xlat16_22;
    u_xlat16_22 = u_xlat16_22 * u_xlat16_22;
    u_xlat16_22 = u_xlat16_22 * 0.5 + 0.0399999991;
    u_xlat16_2 = textureLod(unity_SpecCube0, u_xlat16_1.xyz, 4.05000019);
    u_xlat16_1.x = u_xlat16_2.w + -1.0;
    u_xlat16_1.x = unity_SpecCube0_HDR.w * u_xlat16_1.x + 1.0;
    u_xlat16_1.x = max(u_xlat16_1.x, 0.0);
    u_xlat16_1.x = log2(u_xlat16_1.x);
    u_xlat16_1.x = u_xlat16_1.x * unity_SpecCube0_HDR.y;
    u_xlat16_1.x = exp2(u_xlat16_1.x);
    u_xlat16_1.x = u_xlat16_1.x * unity_SpecCube0_HDR.x;
    u_xlat16_1.xyz = u_xlat16_2.xyz * u_xlat16_1.xxx;
    u_xlat16_2.xyz = u_xlat16_1.xyz * vec3(0.941176474, 0.941176474, 0.941176474);
    u_xlat16_2.xyz = vec3(u_xlat16_22) * u_xlat16_2.xyz;
    u_xlat16_0.w = 1.0;
    u_xlat16_1.x = dot(unity_SHAr, u_xlat16_0);
    u_xlat16_1.y = dot(unity_SHAg, u_xlat16_0);
    u_xlat16_1.z = dot(unity_SHAb, u_xlat16_0);
    u_xlat16_1.xyz = u_xlat16_1.xyz + vs_TEXCOORD6.xyz;
    u_xlat16_1.xyz = max(u_xlat16_1.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat4.xy = vs_TEXCOORD3.xy * vec2(Vector1_FFCA610B, Vector1_E53E3416) + vec2(Vector1_3D95EAF7, Vector1_1B30607B);
    u_xlat16_4.xyz = texture(Texture2D_7005A439, u_xlat4.xy).xyz;
    u_xlat16_5.xyz = u_xlat16_4.xyz * vec3(0.959999979, 0.959999979, 0.959999979);
    u_xlat16_2.xyz = u_xlat16_1.xyz * u_xlat16_5.xyz + u_xlat16_2.xyz;
    u_xlat23 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat23 = max(u_xlat23, 1.17549435e-38);
    u_xlat23 = inversesqrt(u_xlat23);
    u_xlat3.xyz = vec3(u_xlat23) * u_xlat3.xyz;
    u_xlat23 = dot(u_xlat16_0.xyz, u_xlat3.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat23 = min(max(u_xlat23, 0.0), 1.0);
#else
    u_xlat23 = clamp(u_xlat23, 0.0, 1.0);
#endif
    u_xlat3.x = dot(_MainLightPosition.xyz, u_xlat3.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat3.x = min(max(u_xlat3.x, 0.0), 1.0);
#else
    u_xlat3.x = clamp(u_xlat3.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat3.x * u_xlat3.x;
    u_xlat16_3.x = max(u_xlat16_1.x, 0.100000001);
    u_xlat16_1.x = dot(u_xlat16_0.xyz, _MainLightPosition.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_1.x = min(max(u_xlat16_1.x, 0.0), 1.0);
#else
    u_xlat16_1.x = clamp(u_xlat16_1.x, 0.0, 1.0);
#endif
    u_xlat16_1.x = u_xlat16_1.x * unity_LightData.z;
    u_xlat16_1.xyz = u_xlat16_1.xxx * _MainLightColor.xyz;
    u_xlat23 = u_xlat23 * u_xlat23;
    u_xlat23 = u_xlat23 * -0.9375 + 1.00001001;
    u_xlat23 = u_xlat23 * u_xlat23;
    u_xlat23 = u_xlat16_3.x * u_xlat23;
    u_xlat23 = u_xlat23 * 3.0;
    u_xlat23 = 0.0625 / u_xlat23;
    u_xlat16_22 = u_xlat23 + -6.10351563e-05;
    u_xlat16_6.xyz = vec3(u_xlat16_22) * vec3(0.0399999991, 0.0399999991, 0.0399999991) + u_xlat16_5.xyz;
    u_xlat16_1.xyz = u_xlat16_6.xyz * u_xlat16_1.xyz + u_xlat16_2.xyz;
    SV_TARGET0.xyz = vs_TEXCOORD7.yzw * u_xlat16_5.xyz + u_xlat16_1.xyz;
    SV_TARGET0.w = 1.0;
    return;
}

#endif
                              $Globals          _MainLightPosition                           _MainLightColor                             UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          UnityPerMaterial          Vector1_FFCA610B                         Vector1_E53E3416                        Vector1_3D95EAF7                        Vector1_1B30607B                        Vector1_D45CEC2F                        Vector1_92BCA7DF                        Vector1_4F55B5A9                        Vector1_7693A0B9                            $Globals`        _AdditionalLightsCount                           _AdditionalLightsPosition                           _AdditionalLightsColor                         _AdditionalLightsAttenuation                       _AdditionalLightsSpotDir                       _WorldSpaceCameraPos                       unity_MatrixVP                                unity_SpecCube0                   Texture2D_7005A439                  Texture2D_5A3D6B5D                  UnityPerDraw              UnityPerMaterial          