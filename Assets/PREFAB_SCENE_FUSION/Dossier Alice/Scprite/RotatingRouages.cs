﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingRouages : MonoBehaviour
{
    // Update is called once per frame
    Transform lot;
    static public bool yes;
    static public int sens;
    public AudioClip gear;
    public AudioSource zone;

    private void Start()
    {
        zone.clip = gear as AudioClip;
        lot = GetComponent<Transform>();
    }

    public void Update()
    {
        if (yes)
        {
            if (!zone.isPlaying)
            zone.Play();

            foreach (Transform item in lot)
        {

            item.transform.Rotate(new Vector3(360*sens, 0, 0) * (item.transform.localScale.y*Time.deltaTime*0.5f));
        }
        }
    }
    
}
