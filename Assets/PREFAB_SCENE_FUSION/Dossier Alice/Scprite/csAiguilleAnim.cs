﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class csAiguilleAnim : MonoBehaviour
{
    public float AngleX = 0;
    public float AngleY = 0;
    public float AngleZ = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        LeanTween.rotateLocal(gameObject, new Vector3(AngleX, AngleY, AngleZ), 5);
        LeanTween.rotateLocal(gameObject, new Vector3(0, AngleY, AngleZ), 5).setDelay(5);
    }
}
