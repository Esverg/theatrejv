﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class csLampClignot : MonoBehaviour
{
    Transform lot;
    public Material[] ampoule;
    static public bool check;

    private int step = 0;

    private float nextFire;
    // Start is called before the first frame update
    void Start()
    {
        lot = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (check)
        { 
        //Debug.Log("Ptn "+ step);
        if (Time.time > nextFire)
        {
            nextFire = Time.time + .6f;
            foreach (Transform item in lot)
            {
                Renderer Grav = item.GetComponent<Renderer>();
                
                if (item.gameObject.name == "lampe_"+step)
                {
                   Grav.material = ampoule[0];
                }
            }
            step += 1;
        }
        }

        else
        {
            step = 0;
            foreach (Transform item in lot)
            {
                Renderer Grav = item.GetComponent<Renderer>();
                Grav.material = ampoule[1];
            }
        }
    }
}
