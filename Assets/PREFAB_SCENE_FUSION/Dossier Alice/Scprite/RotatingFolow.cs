﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingFolow : MonoBehaviour
{
    public GameObject followedPoint;

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.rotation = new Quaternion(0, followedPoint.transform.rotation.y, 0, followedPoint.transform.rotation.w);
    }
}
