﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StringsFollowing : MonoBehaviour
{
    public GameObject followedPoint;

    void Update()
    {
        gameObject.transform.position = new Vector3(followedPoint.transform.position.x, followedPoint.transform.position.y, followedPoint.transform.position.z+ 0.0022302f);
    }
}
