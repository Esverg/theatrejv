﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour
{
    public int indexStory;

    public enum LightType {  ExteriorLight, InteriorLight }
    public LightType lightType;

    private enum LightMode { Static, TargetObject, RandomRotation }
    private LightMode lightMode;

    public float maxIntensity;
    private Quaternion iniRot;
    public Transform objectTarget;

    private float tIntensity;
    private bool lightActive;

    private StoriesManager storiesManager;

    private void Start()
    {
        storiesManager = FindObjectOfType<StoriesManager>();

        if (lightType == LightType.InteriorLight)
        {
            lightActive = false;
            iniRot = gameObject.transform.localRotation;
            tIntensity = 0f;
        }
    }

    private void Update()
    {
        if (lightType == LightType.InteriorLight && indexStory != storiesManager.storyIndexReference)
        {
            gameObject.GetComponent<Light>().enabled = false;
        }
        else if (lightType == LightType.InteriorLight && indexStory == storiesManager.storyIndexReference)
        {
            gameObject.GetComponent<Light>().enabled = true;

            InteriorLight();
        }

        if (lightType == LightType.ExteriorLight)
        {
            ExteriorLight();
        }
    }

    ///Fonction pour les lumières extérieures
    public void ExteriorLight()
    {
        objectTarget = null;

        //Niveau d'intensité des lumières extérieures selon l'état de l'animation des rideaux
        //float tAnimCurtains = Mathf.InverseLerp(0, maxIntensity, storiesManager.leftCurtain.GetComponent<Animation>()["Scene"].normalizedTime);
        //gameObject.GetComponent<Light>().intensity = Mathf.Lerp(maxIntensity, 0, tAnimCurtains);
    }

    ///Fonction pour les lumières interieures
    public void InteriorLight()
    {
        //Activation selon l'état des scènes
        if (storiesManager.anim.GetBool("PlayStory"))
        {
            lightActive = true;
        }
        else if (!storiesManager.anim.GetBool("PlayStory") && !storiesManager.leftCurtain.GetComponent<Animation>().isPlaying)
        {
            lightActive = false;
        }

        //Activation ou désactivation de la lumière
        if (lightActive)
        {
            if (tIntensity < maxIntensity)
            {
                tIntensity += Time.deltaTime;
            }
        }
        else
        {
            if (tIntensity > maxIntensity * -1)
            {
                tIntensity -= Time.deltaTime;
            }
        }
        float tIntensityLights = Mathf.InverseLerp(0, maxIntensity, tIntensity);
        gameObject.GetComponent<Light>().intensity = Mathf.Lerp(0, maxIntensity, tIntensityLights);

        //Attribution des modes
        if (objectTarget == null)
        {
            lightMode = LightMode.Static;
        }
        else
        {
            lightMode = LightMode.TargetObject;
        }

        //Effet des modes
        if (lightMode == LightMode.Static)
        {
            gameObject.transform.localRotation = iniRot;
        }
        else if (lightMode == LightMode.TargetObject)
        {
            gameObject.transform.LookAt(objectTarget);
        }
    }
}
