﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoriesManager : MonoBehaviour
{
    public Transform pointDeRepère;

    public Transform[] stories;
    public string[] storiesTitles;
    public Button[] buttonsList;
    [HideInInspector] public int storyIndexReference;

    [HideInInspector] public Animator anim;

    
    public GameObject leftCurtain;
    public GameObject rightCurtain;

    public GameObject Aiguille;

    public enum CurtainsStatut { Open, Close }
    public CurtainsStatut curtainsStatut;

    public bool coroutineCanBeCalled;


    public Palette[] palettes;
    public GameObject paletteManagerGaOb;


    public AudioSource player;
    public AudioClip[] ambiance;

    public Animation lightScene;

    private void Start()
    {
        anim = GetComponent<Animator>();
        coroutineCanBeCalled = true;
        palettes = paletteManagerGaOb.GetComponentsInChildren<Palette>();
    }

    //Fonction appelée par les boutons
    public void LoadStory(int storyIndex)
    {
        if (coroutineCanBeCalled)
        {
            StartCoroutine(StoriesManagerEnum(storyIndex));
        }
    }

    private IEnumerator StoriesManagerEnum(int storyID)
    {
        coroutineCanBeCalled = false;

        //Les rideaux déjà fermés
        if (curtainsStatut == CurtainsStatut.Close)
        {
            UnloadStories();
            storyIndexReference = storyID;
            LoadStories();
            InitPalette(storiesTitles[storyID - 1]);
            player.Stop();
            csLampClignot.check = true;
                yield return new WaitForSeconds(3);
            SmokeManager.launch = true;
            player.Play();
            anim.SetBool("OpenCurtains", true);
            OpenCurtains();
            AiguilleSet(0);
            cs_volumeManager.VolUp = true;
            RotatingRouages.yes = true;
            RotatingRouages.sens = 1;
            
            yield return new WaitForSeconds(rightCurtain.GetComponent<Animation>()["Scene"].length/2);
            cs_volumeManager.VolUp = false;
            csLampClignot.check = false;
            RotatingRouages.yes = false;

            for (int i = 0; i < buttonsList.Length; i++)
            {
                buttonsList[i].interactable = true;
            }
            AiguilleSet(1);
            curtainsStatut = CurtainsStatut.Open;
            coroutineCanBeCalled = true;
        }
        //Les rideaux déjà ouverts
        else if (curtainsStatut == CurtainsStatut.Open)
        {
            anim.SetBool("OpenCurtains", false);
            CloseCurtains();
           AiguilleSet(0);
            cs_volumeManager.VolDown = true;
            RotatingRouages.yes = true;
            SmokeManager.launch = true;
            RotatingRouages.sens = -1;
            yield return new WaitForSeconds(rightCurtain.GetComponent<Animation>()["Scene_Reversed"].length / 2);

            UnloadStories();
            RotatingRouages.yes = false;
            cs_volumeManager.VolDown = false;
            AiguilleSet(1);
            storyIndexReference = storyID;

            curtainsStatut = CurtainsStatut.Close;
            coroutineCanBeCalled = true;

            StartCoroutine(StoriesManagerEnum(storyIndexReference));
        }
    }

    private void OpenCurtains()
    {
        ///Gestion de l'animation des rideaux
        //Ouverture du rideau de gauche
        Animation leftCurtainAnim = leftCurtain.GetComponent<Animation>();
        leftCurtainAnim["Scene"].speed = 2f;
        leftCurtainAnim.Play("Scene");
        leftCurtainAnim.transform.parent.localRotation = Quaternion.Euler(0, -90, 0);

        //Ouverture du rideau de droite
        Animation rightCurtainAnim = rightCurtain.GetComponent<Animation>();
        rightCurtainAnim["Scene"].speed = 2f;
        rightCurtainAnim.Play("Scene");
        rightCurtainAnim.transform.parent.localRotation = Quaternion.Euler(0, -90, 0);

        
        lightScene.Play("LightManagerOn");
    }

    private void CloseCurtains()
    {
        ///Gestion de l'animation des rideaux
        //Fermeture du rideau de gauche
        Animation leftCurtainAnim = leftCurtain.GetComponent<Animation>();
        leftCurtainAnim["Scene_Reversed"].speed = 2f;
        leftCurtainAnim.Play("Scene_Reversed");
        leftCurtainAnim.transform.parent.localRotation = Quaternion.Euler(0, -90, 0);

        //Fermeture du rideau de droite
        Animation rightCurtainAnim = rightCurtain.GetComponent<Animation>();
        rightCurtainAnim["Scene_Reversed"].speed = 2f;
        rightCurtainAnim.Play("Scene_Reversed");
        rightCurtainAnim.transform.parent.localRotation = Quaternion.Euler(0, -90, 0);


        lightScene.Play("LightManagerOff");
    }

    private void AiguilleSet(int init)
    {
        Animation aiguilleAnim = Aiguille.GetComponent<Animation>();

        if (init == 1)
        {

            aiguilleAnim.Play("RetourAiguille");
        }
        if (init == 0)
        {

            aiguilleAnim.Play("InitAiguille");
        }
    }


    private void LoadStories()
    {
        //Activation des élèments graphiques de l'histoires selectionnée


        stories[storyIndexReference - 1].gameObject.SetActive(true);
        player.clip = ambiance[storyIndexReference - 1] as AudioClip;
        foreach (Transform item in stories[storyIndexReference - 1])
        {
            if (item != stories[storyIndexReference - 1])
            {
            // item.gameObject.SetActive(true);
             Instantiate(item, pointDeRepère.transform);
            }
        }

        //stories[storyIndexReference - 1].gameObject.SetActive(true);
        

        //Gestion des animations de l'histoire selectionnée
        anim.SetInteger("CurrentStory", storyIndexReference);
        anim.SetBool("PlayStory", true);
        anim.SetBool("TitleOpen", true);
        anim.SetBool("LoopStoryMain", false);
    }

    private void UnloadStories()
    {
       // for (int i = 0; i < stories.Length; i++)
        //{
           // stories[i].gameObject.SetActive(false);
            //  Destroy(stories[i].gameObject);
            //  Destroy(pointDeRepère.FindChild())
            //}

           foreach (Transform child in pointDeRepère)
            {
                Destroy(child.gameObject);
            }

        //}

    }

    //Fonctions appelées par des animations
    public void TitleOpen()
    {
        anim.SetBool("TitleOpen", false);
        anim.SetBool("OpenCurtains", true);
    }

    public void FinishedStory()
    {
        anim.SetBool("PlayStory", false);
    }

    public void MainStoryLoop()
    {
        anim.SetBool("LoopStoryMain", true);
    }

    public void InitPalette(string title)
    {
        foreach (var item in FindObjectsOfType<Palette>())
        {
            item.StopAllCoroutines();
            item.Refresh();
        }

        for (int i = 0; i < title.Length; i++)
        {
            palettes[i].StartCoroutine(palettes[i].RotatePalette(title[i]));
        }
    }

}
