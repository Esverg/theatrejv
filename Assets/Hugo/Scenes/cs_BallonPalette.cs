﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cs_BallonPalette : MonoBehaviour
{
    public Transform[] décor;

    public Transform spawnDécor;

    public int actual;

    public float spawnRate = 1;
    private float nextSpawn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > nextSpawn)
        {
          //  décor[actual].gameObject.SetActive(true);
            cs_décorMovement test = décor[actual].GetComponent<cs_décorMovement>();
            test.imRunning = true;
            actual += 1;
            nextSpawn = Time.time + spawnRate;
        }

        if (actual > décor.Length -1)
        {
            actual = 0;
        }
    }
}
