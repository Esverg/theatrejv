﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmokeManager : MonoBehaviour
{
    [Header("PrimaryVariable")]
    public ParticleSystem smoke;
    public GameObject[] spot;
    [Header("SecondaryVariable")]
    public float Delay;

    public static bool launch = false;

    public AudioClip steam;
    public AudioSource zone;

    private void Start()
    {
        zone.clip = steam as AudioClip;
    }

    private void Update()
    {
        //Input for create smoke instance
        //   if (Input.GetKeyUp(KeyCode.F))
        //  {
        //    StartCoroutine(InitSmoke());
        //  }

        if (launch)
        {
            StartCoroutine(InitSmoke());
            launch = false;
        }
    }

    public IEnumerator InitSmoke()
    {
        foreach (var item in spot)
        {
            //Storage of the smoke instance into variable ps 
            ParticleSystem ps = Instantiate(smoke, item.transform.position, Quaternion.Euler(item.transform.eulerAngles));
            ps.transform.parent = item.transform;
            ps.transform.localScale = new Vector3(0.06902222f, 0.06902222f, 0.06902222f);
            zone.Play();
            //Can use some delay for each smoke instance
            yield return new WaitForSeconds(Delay);
            Destroy(ps.gameObject);

        }
    }


}
