﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaletteManager : MonoBehaviour
{
    public Palette[] palettes;
    public GameObject instance;

    void Start()
    {
        palettes = GetComponentsInChildren<Palette>();
    }

    public void InitPalette(string title)
    {
        foreach (var item in FindObjectsOfType<Palette>())
        {
            item.StopAllCoroutines();
            item.Refresh();
        }

        for (int i = 0; i < title.Length; i++)
        {
            palettes[i].StartCoroutine(palettes[i].RotatePalette(title[i]));
        }
    }
}