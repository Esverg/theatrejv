﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cs_volumeManager : MonoBehaviour
{
    private AudioSource player;
    public static bool VolUp;
    public static bool VolDown;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public void Update()
    {
        if (VolUp)
            player.volume += 0.001f;
        else if (VolDown)
            player.volume -= 0.001f;
    }

}
